const gridCell = document.querySelectorAll('.cell');

const currentStatus = document.querySelector('.status');

const reset = document.querySelector('.restart');


let gameActive = true;
let currentPlayer = 'X';

//Messages Functions

const tieMessage = () => "Its tie";

const statusMessage = () => `Its ${currentPlayer} Turn`;

const wonMessage = (player) => `Player ${player} has Won!!`;



// handling Winning

function handleWon(player){
    
    gameActive = false;
    currentStatus.innerHTML = wonMessage(player);
    gridCell.forEach(cell => {
        cell.removeEventListener("click", handleCellClick)
    })
}

// Check Status

function checkStatus() {

    const cell_0 = gridCell[0].classList[2];
    const cell_1 = gridCell[1].classList[2];
    const cell_2 = gridCell[2].classList[2];
    const cell_3 = gridCell[3].classList[2];
    const cell_4 = gridCell[4].classList[2];
    const cell_5 = gridCell[5].classList[2];
    const cell_6 = gridCell[6].classList[2];
    const cell_7 = gridCell[7].classList[2];
    const cell_8 = gridCell[8].classList[2];

    const x_node = document.querySelectorAll('.x');
    const o_node = document.querySelectorAll('.o');

    if(cell_0 && cell_0 === cell_1 && cell_0 === cell_2){
     
        handleWon(cell_0);
    
    } else if(cell_3 && cell_3 === cell_4 && cell_3 === cell_5){

        handleWon(cell_3);
    
    }  else if(cell_6 && cell_6 === cell_7 && cell_6 === cell_8){

        handleWon(cell_6);
    
    } else if(cell_0 && cell_0 === cell_3 && cell_0 === cell_6){
     
        handleWon(cell_0);
    
    } else if(cell_1 && cell_1 === cell_4 && cell_1 === cell_7){

        handleWon(cell_1);
    
    }  else if(cell_2 && cell_2 === cell_5 && cell_2 === cell_8){

        handleWon(cell_2);
    
    } else if(cell_0 && cell_0 === cell_4 && cell_0 === cell_8){
     
        handleWon(cell_0);
    
    } else if(cell_2 && cell_2 === cell_4 && cell_2 === cell_6){

        handleWon(cell_2);    
 
    } else if(x_node.length === 5 && o_node.length===4){
        currentStatus.innerHTML = tieMessage();
    } 

}



// Event handlers

function restart(e){
    location.reload();
}

function handleCellClick(e){

    if(currentPlayer === 'X'){
        e.target.classList.add('x');
        currentPlayer = 'O';
        currentStatus.innerHTML = statusMessage();
        checkStatus();
        e.target.style.pointerEvents = "none";   
    
    } else {
        e.target.classList.add('o');
        currentPlayer = 'X';
        currentStatus.innerHTML = statusMessage();
        checkStatus();
        e.target.style.pointerEvents = "none";
    }

}

// Events

reset.addEventListener("click", restart)

for(let cell of gridCell){
    cell.addEventListener("click", handleCellClick);
}    

